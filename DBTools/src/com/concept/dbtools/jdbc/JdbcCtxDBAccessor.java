package com.concept.dbtools.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Priority;
import javax.enterprise.inject.Alternative;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.concept.dbtools.DBAccessor;
import com.concept.dbtools.DBConfig;

@Priority(2)
@Alternative
public class JdbcCtxDBAccessor implements DBAccessor {

	public static Boolean TESTING = Boolean.FALSE;
	public static String DS_NAME = "";
	private DataSource ds;
	
	@Override
	public void setup(DBConfig config) {
		DS_NAME = ((JdbcContextConfig)config).getDsName();		
	}
	
	public Connection connect() throws SQLException {

		if (DS_NAME.isEmpty()) {
			throw new SQLException("Datasource has not been initialized!");
		}
		
		Connection conn = null;
		try {
			Context envCtx = new InitialContext();

			ds = (DataSource) envCtx.lookup(DS_NAME);
			conn = ds.getConnection();

		} catch (NamingException e) {
			// Is testing enabled
			if (!TESTING) {
				e.printStackTrace();
				throw new SQLException(e);
			}
		}

		return conn;
	}

	public void disconnect(Connection connection, PreparedStatement rs) {
		if( rs != null ) {
			try { rs.close(); } catch(Exception e) {}
		}
		rs = null;
		if( connection != null ) {
			try { connection.close(); } catch(Exception e) {}
		}
		connection = null;
	}

	public void disconnect(Connection connection) {
		disconnect(connection, null);
	}

	@Override
	public int getGenerateKey(PreparedStatement stmt) throws SQLException {
		ResultSet rs = stmt.getGeneratedKeys();
		rs.next();
		int id = rs.getInt(1);
		return id;
	}

	@Override
	public int getGenerateKey(PreparedStatement stmt, String columnName) throws SQLException {
		ResultSet rs = stmt.getGeneratedKeys();
		int id = 0;
		while (rs.next()){
			try {
				id = rs.getInt(columnName);
			} catch (Exception e){}
		}
		return id;
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DataSource getDataSource() {
		return ds;
	}

	@Override
	public void runUpdate(Connection connection, String sql) throws SQLException {
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.executeUpdate();
		}  finally  {
			this.disconnect(connection);
		}
	}

}
