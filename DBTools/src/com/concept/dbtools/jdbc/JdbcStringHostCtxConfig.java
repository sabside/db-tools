package com.concept.dbtools.jdbc;

import com.concept.dbtools.DBConfig;

public class JdbcStringHostCtxConfig implements DBConfig {

    private final String host;
    private final Integer port;
    private final String username;
    private final String password;
    private final String database;
    private final Boolean ssl;
    private final String schema;

    public JdbcStringHostCtxConfig(String host, Integer port, String username, String password, String database, Boolean ssl,  String schema) {
        super();
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.database = database;
        this.ssl = ssl;
        this.schema = schema;
    }

    public String getHost() {
        return host;
    }

    public Integer getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDatabase() {
        return database;
    }

    public Boolean getSsl() {
        return ssl;
    }

	public String getSchema() {
		return schema;
	}
}
