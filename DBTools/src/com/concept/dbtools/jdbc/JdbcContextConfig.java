package com.concept.dbtools.jdbc;

import com.concept.dbtools.DBConfig;

/**
 * 
 * @author Sabside
 *
 */
public class JdbcContextConfig implements DBConfig {

	private final String dsName;

	public JdbcContextConfig(String dsName) {
		super();
		this.dsName = dsName;
	}

	public String getDsName() {
		return dsName;
	}

}
