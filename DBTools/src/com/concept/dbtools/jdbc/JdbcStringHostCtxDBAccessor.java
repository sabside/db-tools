package com.concept.dbtools.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Priority;
import javax.enterprise.inject.Alternative;
import javax.sql.DataSource;

import com.concept.dbtools.DBAccessor;
import com.concept.dbtools.DBConfig;

@Priority(1)
@Alternative
public class JdbcStringHostCtxDBAccessor implements DBAccessor {

    public static Boolean TESTING = Boolean.FALSE;

    private static String host;
    private static Integer port ;
    private static String username;
    private static String password;
    private static String database;
    private static Boolean ssl;
    private static String schema;
    private String URL = "jdbc:postgresql://%s:%d/%s";
    
    @Override
    public void setup(DBConfig config) {
        host = ((JdbcStringHostCtxConfig)config).getHost();
        port = ((JdbcStringHostCtxConfig)config).getPort();
        username = ((JdbcStringHostCtxConfig)config).getUsername();
        password = ((JdbcStringHostCtxConfig)config).getPassword();
        database = ((JdbcStringHostCtxConfig)config).getDatabase();
        ssl = ((JdbcStringHostCtxConfig)config).getSsl();
        schema = ((JdbcStringHostCtxConfig)config).getSchema();
    }

    @Override
    public Connection connect() throws SQLException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new SQLException(e);
        }
        String url = String.format(URL, host, port, database);
        if (schema != null) url = url.concat("?currentSchema=").concat(schema);
        return DriverManager.getConnection(url, username, password);
    }

    @Override
    public void disconnect(Connection connection, PreparedStatement rs) {
        if( rs != null ) {
            try { rs.close(); } catch(Exception e) {}
        }
        rs = null;
        if( connection != null ) {
            try { connection.close(); } catch(Exception e) {}
        }
        connection = null;
    }

    @Override
    public void disconnect(Connection connection) {
        disconnect(connection, null);
    }

    @Override
    public int getGenerateKey(PreparedStatement stmt) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        rs.next();
        int id = rs.getInt(1);
        return id;
    }

    @Override
    public int getGenerateKey(PreparedStatement stmt, String columnName) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        int id = 0;
        while (rs.next()){
            try {
                id = rs.getInt(columnName);
            } catch (Exception e){}
        }
        return id;
    }

    @Override
    public void init() {

    }

	@Override
	public DataSource getDataSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void runUpdate(Connection connection, String sql) throws SQLException {
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.executeUpdate();
		}  finally  {
			this.disconnect(connection);
		}
	}
}
