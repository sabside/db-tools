package com.concept.dbtools.jdbc;

import java.util.Arrays;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.concept.dbtools.DBAccessor;
import com.concept.dbtools.DBConfig;

public class ConfigFactory {
	private static final Logger log = Logger.getLogger(ConfigFactory.class.getName());
	
	public static DBConfig createConfig(String key, DBAccessor instance) {
		log.info(instance.getClass().getName()+" is the instance!!!");
		if (instance instanceof JdbcCtxDBAccessor) {
			log.info("JdbcCtxDBAccessor instantiate");
			return new JdbcContextConfig(key);
		} else if (instance instanceof JdbcStringHostCtxDBAccessor) {
			log.info("JdbcStringHostCtxDBAccessor instantiate");
			
			Map<String, String> map = Arrays.asList(key.split(","))
										.stream()
										.map(keypair->{
											return keypair.split("=");
										})
										.collect(Collectors.toMap(item -> item[0], item -> item[1]));
			
			JdbcStringHostCtxConfig config = new JdbcStringHostCtxConfig(map.get("host"), Integer.parseInt(map.get("port")), map.get("username"), map.get("password"), map.get("database"), 
												Boolean.parseBoolean(map.get("ssl")), map.get("schema"));
			
			return config;
		}
		
		log.severe(instance.getClass().getName()+" does not implement DBAccessor!!!");
		
		return null;
	}
}
