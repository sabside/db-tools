package com.concept.dbtools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 * 
 * @author Sabelo Simelane [F3557790]
 *
 */
@Stateless
public interface DBAccessor {

	void init();
	
	void setup(DBConfig config);
	
	Connection connect() throws SQLException;

	void disconnect(Connection connection, PreparedStatement query);

	void disconnect(Connection connection);
	
	int getGenerateKey(PreparedStatement stmt) throws SQLException;
	
	int getGenerateKey(PreparedStatement stmt, String columnName) throws SQLException;
	
	DataSource getDataSource();
	
	void runUpdate(Connection connection, String sql) throws SQLException;
}
