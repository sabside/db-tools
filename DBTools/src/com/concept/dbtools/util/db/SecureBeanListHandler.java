package com.concept.dbtools.util.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.ResultSetHandler;

public class SecureBeanListHandler<T> implements ResultSetHandler<List<T>> {
	private final Class<T> type;

	public SecureBeanListHandler(Class<T> type) {
		this.type = type;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<T> handle(ResultSet paramResultSet) throws SQLException {

		ResultSetListConverter converter = new ResultSetListConverter<>();
		List<T> result = null;
		try {
			result = converter.resultSetToObject(paramResultSet, type);
		} catch (IndexOutOfBoundsException e) {
			result = new ArrayList<>();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}


}
