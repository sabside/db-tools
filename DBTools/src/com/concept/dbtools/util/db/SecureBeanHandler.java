package com.concept.dbtools.util.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.ResultSetHandler;

/**
 * 
 * @author Sabelo Simelane [F3557790]
 *
 * @param <T>
 */
public class SecureBeanHandler<T> implements ResultSetHandler<T> {

	private final Class<T> type;

	public SecureBeanHandler(Class<T> type) {
		this.type = type;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public T handle(ResultSet paramResultSet) throws SQLException {

		ResultSetBeanConverter converter = new ResultSetBeanConverter<>();
		T result = null;
		try {
			result = (T) converter.resultSetToObject(paramResultSet, type);
		} catch (IndexOutOfBoundsException e) {
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new SQLException(e);
		}

		return result;
	}

}
