package com.concept.dbtools.util.db;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ResultSetListConverter<T> {
	
	private static final Logger log = Logger.getLogger(ResultSetListConverter.class.getName());
	
	boolean constructorFound = false;
	
	@SuppressWarnings("unchecked")
	public List<T> resultSetToObject(ResultSet rs, Class<T> class1) throws InstantiationException, IllegalAccessException{
		
		List<T> result = new ArrayList<>();
		
		Class<? extends Object> clazz = class1;
		
		List<T> optionalResult = Arrays.asList(clazz.getConstructors())
		.stream()
		// We sort by parm count, since we need to match the most specific constructor.
		// i.e We'll try to match a constructor with 9 parms, before we match once with 8
		.sorted((i1,i2)->{
			return Integer.compare(i1.getParameterCount(),i2.getParameterCount());
		}).filter(constructor->{
			
			// Populate the Contructor parm names
			Map<String, Parameter> parmsAndOrder = new LinkedHashMap<>();
			Arrays.asList(constructor.getParameters()).forEach(parm->{
				parmsAndOrder.put(parm.getName(),parm);
			});
			
			// Get a list of field names that are present in the result set.
			// We need to try and match fields from the RS, to matching fields in the Constructor
			List<String> databaseFieldNames = new ArrayList<>();
			try {
				ResultSetMetaData rsmd = rs.getMetaData();
				for(int i=1;i <= rsmd.getColumnCount();i++){
					databaseFieldNames.add(rsmd.getColumnLabel(i).toLowerCase().replaceAll("_", ""));
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			// Check if we have all the fields needed for the Constructor
			StringBuffer buf = new StringBuffer();
			buf.append("(");
			parmsAndOrder.forEach((key, val)->{
				buf.append(key).append(", ");
			});
			buf.append(")");
			log.info(buf.toString());
			
			boolean shouldUseConstructor = parmsAndOrder.keySet().stream().allMatch(keyName->databaseFieldNames.contains(keyName.toLowerCase()));
			if (shouldUseConstructor){
				constructorFound = true;
				return true;
			}
			return false;
		})
		// Convert to concrete object 
		.map(constructor->{
			
			T r1 = null;
			try {
	
				while (rs.next()){
					int numOfCols = rs.getMetaData().getColumnCount(); 
					Object[] constructorObjects = new Object[constructor.getParameterCount()];
					
					Map<String, Integer> parmOrder = new HashMap<>(); 
					for (int j=0; j<constructorObjects.length; j++){
						parmOrder.put(constructor.getParameters()[j].getName().toLowerCase(), j);
					}
					
					for (int i =1; i<numOfCols+1; i++){
						String columnName = rs.getMetaData().getColumnLabel(i).toLowerCase();
						if (columnName.contains("_")){
							columnName = columnName.replaceAll("_", "");
						}
						
						Object value = rs.getObject(i);
						//System.out.println("column: "+parmOrder.get(columnName)+" > "+columnName +" > "+rs.getMetaData().getColumnTypeName(i));
						if (rs.getMetaData().getColumnTypeName(i).contains("int") && value == null){
							value = 0;
						} else if (rs.getMetaData().getColumnTypeName(i).contains("text") && value == null){
							value = "";
						} else if (rs.getMetaData().getColumnTypeName(i).contains("bool") && value == null){
							value = false;
						}
						constructorObjects[parmOrder.get(columnName)] = value;
					}
					
					r1 = (T) constructor.newInstance(constructorObjects);
					result.add(r1);
				}
				
			} catch (SQLException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {

				e.printStackTrace();
				
			}
			
			return r1;

		}).collect(Collectors.toList());
	
		if (!constructorFound) throw new InstantiationException("No suitable constructor found for DB Resulset!");
		
		return result ;
	}

}
